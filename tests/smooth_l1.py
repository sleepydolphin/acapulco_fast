#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '..')

import numpy as np
from keras import backend as K

from keras_unet_cerebellum.smooth_l1 import calc_smooth_l1_loss

y_true = np.array([50, 30, 40, 20, 10, 60])
y_pred = np.array([48, 30.5, 43, 29.8, 5, 60.1])

d = calc_smooth_l1_loss(y_true, y_pred)
print(K.eval(d))
