# -*- coding: utf-8 -*-


import numpy as np
from keras.layers import Input, LeakyReLU, Add, UpSampling3D, Conv3D
from keras.layers import GlobalAveragePooling3D, BatchNormalization, Flatten
from keras.models import Model
from network_utils.data_factories import Data3dFactoryDecorator
from network_utils.data_decorators import DataDecorator
from image_processing_3d import calc_bbox3d


class BoundingBoxFactory:

    def __init__(self, input_factory, encoder_factory, output_factory,
                 max_num_features=1024):
        self.input_factory = input_factory
        self.encoder_factory = encoder_factory
        self.output_factory = output_factory
        self.max_num_features = max_num_features

    def create(self, input_shape, num_input_block_features, num_encoders,
               num_values=6):

        input = Input(shape=input_shape)
        output = self.input_factory.create(input_shape,
                                           num_input_block_features)(input)

        output_shape = output.shape[1:].as_list() # remove batch dim
        num_out_features = num_input_block_features
        for i in range(num_encoders):
            output = self.encoder_factory.create(output_shape,
                                                 num_out_features,
                                                 use_dropout=True)(output)
            output_shape = output.shape[1:].as_list()
            num_out_features = self._get_num_features(2 * num_out_features)
        
        output = self.output_factory.create(output_shape, num_values)(output)
        model = Model(inputs=input, outputs=output)

        return model

    def _get_num_features(self, num_features):
        return min(num_features, self.max_num_features)


class BboxFactoryDecorator(Data3dFactoryDecorator):

    def _create_none(self, filepaths):
        result = self._calc_bbox(self.factory.data['none'])
        self.data['none'] = result

    def _create_flipped(self):
        result = self._calc_bbox(self.factory.data['flipped'])
        self.data['flipped'] = result

    def _create_rotated(self):
        result = self._calc_bbox(self.factory.data['rotated'])
        self.data['rotated'] = result

    def _create_deformed(self):
        result = self._calc_bbox(self.factory.data['deformed'])
        self.data['deformed'] = result

    def _create_scaled(self):
        result = self._calc_bbox(self.factory.data['scaled'])
        self.data['scaled'] = result

    def _create_translated(self):
        result = self._calc_bbox(self.factory.data['translated'])
        self.data['translated'] = result

    def _create_translated_flipped(self):
        result = self._calc_bbox(self.factory.data['translated_flipped'])
        self.data['translated_flipped'] = result

    def _create_rotated_flipped(self):
        result = self._calc_bbox(self.factory.data['rotated_flipped'])
        self.data['rotated_flipped'] = result

    def _create_deformed_flipped(self):
        result = self._calc_bbox(self.factory.data['deformed_flipped'])
        self.data['deformed_flipped'] = result

    def _create_scaled_flipped(self):
        result = self._calc_bbox(self.factory.data['scaled_flipped'])
        self.data['scaled_flipped'] = result

    def _calc_bbox(self, data):
        image = data[0]
        bbox = Bbox(data[1], get_data_on_the_fly=data[1].get_data_on_the_fly)
        return image, bbox


class Bbox(DataDecorator):

    def _get_data(self):
        bbox_slices = calc_bbox3d(self.data.get_data()[0, ...])
        bbox = list()
        for s in bbox_slices:
            bbox.append(s.start)
            bbox.append(s.stop)
        return np.array(bbox)
