#!/usr/bin/env bash

if [ -z $ALPHA ]; then
    ALPHA=1
fi
if [ -z $NUM_SLICES ]; then
    NUM_SLICES=10
fi
if [ -z $TMC_BBOX_MODEL ]; then
    TMC_BBOX_MODEL=/opt/models/tmc_bbox_model.h5
fi
if [ -z $TMC_PARC_MODEL ]; then
    TMC_PARC_MODEL=/opt/models/tmc_parc_model.h5
fi
if [ -z $KKI_BBOX_MODEL ]; then
    KKI_BBOX_MODEL=/opt/models/kki_bbox_model.h5
fi
if [ -z $KKI_PARC_MODEL ]; then
    KKI_PARC_MODEL=/opt/models/kki_parc_model.h5
fi
if [ -z $KKI_LABELS ]; then
    KKI_LABELS=/opt/kki_labels.json
fi
if [ -z $TMC_LABELS ]; then
    TMC_LABELS=/opt/tmc_labels.json
fi


function usage() {
    echo "Parcellate a cerebellum with ACAPULCO."
    echo ""
    echo "Usage: acapulco.sh -i IMAGE -m MODEL -o OUTPUT_DIR"
    echo "Args:"
    echo "    -i IMAGE: The input MPRAGE image"
    echo "    -o OUTPUT_DIR: The output directory. The MNI aligned image is "
    echo "        OUTPUT_DIR/mni/{image}_n4_mni.nii.gz, the parcellation is"
    echo "        OUTPUT_DIR/parc/{image}_n4_mni_seg.nii.gz, the post"
    echo "        processed is OUTPUT_DIR/parc/{image}_n4_mni_seg_post.nii.gz,"
    echo "        and the parcellation in the original space is"
    echo "        OUTPUT_DIR/{image}_n4_mni_seg_post_inverse.nii.gz. A report is"
    echo "        generated in OUTPUT_DIR/pics."
    echo "    -m MODEL: Select trained models (-m kki or -m tmc). Default is tmc"
    echo "    -h: Print help"
}

model=tmc
use_mni_cer_mask=false
while getopts ":hi:o:m:u" opt; do
    case ${opt} in
    h)
        usage
        exit 0
        ;;
    i)
        image=$OPTARG
        ;;
    o)
        output_dir=$OPTARG
        ;;
    m)
        model=$OPTARG
        if [ $model != "tmc" ] && [ $model != "kki" ]; then
            echo "Invalid -m. Use -m kki or -m tmc." 1>&2
        fi
        ;;
    u)
        use_mni_cer_mask=true
        ;;
    \?)
        echo "Invalid Option: -$OPTARG." 1>&2
        exit 1
        ;;
    :)
        echo "Invalid option: $OPTARG requires an argument." 1>&2
        ;;
    esac
done

if [ $model == "tmc" ]; then
    BBOX_MODEL=$TMC_BBOX_MODEL
    PARC_MODEL=$TMC_PARC_MODEL
    LABELS=$TMC_LABELS
    shape=(182 218 182)
    bbox_shape=(160 96 96)
elif [ $model == "kki" ]; then
    BBOX_MODEL=$KKI_BBOX_MODEL
    PARC_MODEL=$KKI_PARC_MODEL
    LABELS=$KKI_LABELS
    shape=(193 229 193)
    bbox_shape=(128 96 96)
fi

echo Cerebellum parcellation...
mni_image=$image
parc_dir=$output_dir

if $use_mni_cer_mask; then
    $(dirname $0)/predict.py -i $mni_image -o $parc_dir -l $LABELS \
        -p $PARC_MODEL -s ${shape[@]} -c ${bbox_shape[@]} \
        -u $MNIDIR/cerebellum_mask.nii.gz
else
    $(dirname $0)/predict.py -i $mni_image -o $parc_dir -l $LABELS -b $BBOX_MODEL \
        -p $PARC_MODEL -s ${shape[@]} -c ${bbox_shape[@]}
fi
parc=$parc_dir/$(basename $mni_image | sed "s/\.nii/_seg.nii/")
echo "    From $mni_image to $parc"

echo Post processing...
post=$parc_dir/$(basename $mni_image | sed "s/\.nii/_seg_post.nii/")
cleanup_label_image.py -i $parc -o $post
echo "    From $parc to $post"


